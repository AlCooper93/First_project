// TABS

let tabBTN = document.querySelector(".listServices");

tabBTN.addEventListener('click', fTabs);

function fTabs(event) {
    console.log(event);
    let dataTab = event.target.getAttribute('data-tab');

    let tabClick = document.getElementsByClassName('addClass');
    for (let i = 0; i < tabClick.length; i++) {
        tabClick[i].classList.remove('activicon')
        console.log(tabClick[i])
    }

    event.target.classList.add('activicon')

    let tabBody = document.getElementsByClassName('tab-text');
    for (let i = 0; i < tabBody.length; i++) {
        if (dataTab == i) {
            tabBody[i].style.display = "flex"
        } else {
            tabBody[i].style.display = "none"
        }
    }
}


// filter

let portfolioBTN = document.querySelector('.portfolioList');
let PortfolioGalery = document.querySelector('.PortfolioGalery');

portfolioBTN.addEventListener('click', filterPortfolio);

function filterPortfolio(e) {
    console.log(e);
    let filterList = e.target.getAttribute('data-filter');
    let img = document.getElementsByClassName('GrayLayr');

    for (let i = 0; i < img.length; i++) {
        if (img[i].classList.contains(filterList)) {
            img[i].style.display = ""
        } else {
            img[i].style.display = "none"
        }
    }
}

// Tablet Menu

let clickMenu = document.querySelector('.menuTablet');

clickMenu.addEventListener('click', openTabletMenu);

function openTabletMenu(e) {
    let menu = document.querySelector('.open-tablet-menu');
    menu.style.display = menu.style.display === 'block' ? 'none' : 'block'
}

// Wrapper 

let btnNext = document.querySelector('.btn-next');
let btnBack = document.querySelector('.btn-back');
let list = document.querySelector(".listWrap");

btnNext.addEventListener('click', () => {
    let margin = parseInt(list.style.marginLeft);
    console.log(list.style.marginLeft)
    if (margin > -200) {
        let newMagin = margin - 100;
        list.style.marginLeft = newMagin + "vw";
    }
})

btnBack.addEventListener('click', () => {
    let margin = parseInt(list.style.marginLeft);
    console.log(list.style.marginLeft)
    if (margin < 100) {
        let newMagin = margin + 100;
        list.style.marginLeft = newMagin + "vw";
    }
})

// MAIL 

let errorMail = document.querySelector('.errorMail');
let errorName = document.querySelector('.errorName');
let welcome = document.querySelector('.welcome');
let submit = document.querySelector('.submit');
let email = document.querySelector("input[type='email']");
let name = document.querySelector(".jsClass");
console.log(name)
submit.addEventListener('click', correctText);

function correctText() {
    welcome.style.display = 'none';
    errorName.style.display = "none";
    errorMail.style.display = "none";
    let srcName = name.value;
    if (srcName.length >= 1) {
        correctEmail()
    }
    else {
        errorName.style.display = "block";
    }
}

function correctEmail() {
    let srcMail = email.value;
    if (srcMail.length >= 5 && srcMail.length <= 256 && srcMail.substring(0, 1) !== "@" && srcMail.substr(-1) !== "@" && srcMail.search('@') != -1 ) {
        welcome.style.display = 'block';
    }
    else {
        errorMail.style.display = "block";
    }
}
